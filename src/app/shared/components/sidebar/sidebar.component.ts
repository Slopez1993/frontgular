import { Component } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';

import { SHOW_COLLECTIONS, SIDEBAR_LINKS } from '../../../../admin.config';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
})
export class SidebarComponent {

  public collections = SHOW_COLLECTIONS;
  public sidebarLinks = SIDEBAR_LINKS;

  constructor(
    private router: Router
  ) {

  }


}
