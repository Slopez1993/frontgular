export let SHOW_COLLECTIONS = [
  {
    name: 'users',
    label: 'Users',
    icon: 'icon-people',
    color: '#20a8d8',
    tableColumns: [
      { label: 'Id', name: 'id' },
      { label: 'Email', name: 'email' }
    ],
    role: 'super'
  },
  {
    name: 'roles',
    label: 'Roles',
    icon: 'icon-target',
    color: '#d84a20',
    tableColumns: [
      { label: 'Role', name: 'name' },
    ],
    role: 'super'
  }
];

export let SIDEBAR_LINKS = [
  {
    label: 'Pages',
    routerLink: null,
    childs: [
      { label: 'Login', icon:'icon-login', routerLink: '/pages/login' },
      { label: 'Register', icon:'icon-notebook', routerLink: '/pages/register' },
      { label: 'Error 404', icon:'icon-info', routerLink: '/pages/404' },
      { label: 'Error 400', icon:'icon-info', routerLink: '/pages/500' },
    ]
  }

];
